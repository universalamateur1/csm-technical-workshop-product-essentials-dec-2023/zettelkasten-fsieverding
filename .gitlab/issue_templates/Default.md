## Description and goal

_**Tip**: Explain your motivations and goals so that whenever you takes charge, you can think of possible alternative solutions, if necessary._

## Tasks and activities

* [ ] Task 1
* [ ] Task 2
* ...

## Acceptance criteria

* [ ] Definition of Done
* [ ] Iterated

/label ~"GTD::Inbox"
/assign @fsieverding

<!-- possible
/label ~"Prio::low"
r
/due in 14 days
 !-->
